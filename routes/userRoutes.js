const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");

const auth = require("../auth")

//Route for checking if the user's email already exists in the database
//Invoke the checkEmailExists function from the controller file later to communicate with our database
//passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

//Route for user registration

router.post("/register",(req,res)=>{

	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))

});

//Route for user authentication

router.post("/login",(req,res)=>{

	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});

/*
	1. Create a /details route that will accept the user's Id to retrieve the details of a user
	2. Create a getProfile controller method for retrieving the details of the user:
		a. Find the document in the database using the user's ID
		b. Reassign the password of the returned document to an empty string
		c. Return the result back to the frontend
	3. Process a post request at the /details route using postman to retrieve the details of the user	
*/
/*
router.post("/details", (req, res) => {

	userController.getProfile(req.body.id).then((resultFromController) => {
		res.send(resultFromController);
	});
});
*/

router.get("/details",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));

})


//Route to enroll user to a course

/*router.post("/enroll", (req,res)=>{

	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController=>res.send(resultFromController));

})
*/

router.post("/enroll", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization);
    let data = {
        userId: userData,
        courseId: req.body.courseId
    }
    userController.enroll(data).then(resultFromUserController => res.send(resultFromUserController));

});



//Allows us to export the router object that will be accessed in our "index.js"

module.exports = router;
