const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

//Route for creating a course

router.post("/",auth.verify,(req,res)=>{


		//data.course.name
		//data.isAdmin	

	const data = {

		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}

	courseController.addCourse(data).then(resultFromController=>res.send(resultFromController))

})

//Route for retrieving all courses

router.get("/all",(req,res)=>{

	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController));
});

//Route for retrieving all the ACTIVE courses

router.get("/",(req,res)=>{

	courseController.getAllActive().then(resultFromController=>res.send(resultFromController));
});


//Route for updating a course
//JWT verification is need for this route to ensure that the user is logged in before updating a course

router.put("/:courseId", auth.verify, (req,res)=>{

	courseController.updateCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController));
});

//Archive a course
//In managing db, it is a common practice to soft delete our records and what we would implement in the "delete" operation of our application

router.patch("/:courseId/archive", auth.verify, (req, res) => {
  courseController.archiveCourse(req.params).then((resultFromController) => {
    res.send(resultFromController);
  });
});

//Stretch goal

router.patch("/:courseId/unarchive", auth.verify, (req, res) => {
  courseController.unarchiveCourse(req.params).then((resultFromController) => {
    res.send(resultFromController);
  });
});


//allows us to export the "router" object that will be accessed in our "index.js"
module.exports = router;