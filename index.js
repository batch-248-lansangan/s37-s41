const express = require("express");
const mongoose = require("mongoose");

//allows our backend to be available to our frontend application

const cors = require("cors");

//allows access to routes defined within our application
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

//allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database Connection
	//Connect to MongoDB Atlas

mongoose.set('strictQuery',true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.5padyji.mongodb.net/booking-api?retryWrites=true&w=majority",
	{

	useNewUrlParser: true,
	useUnifiedTopology: true

	}
);

//connection to database
let db = mongoose.connection;
db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"))

//defines the /users string to be included for all user routes defined in the userRoutes.js
app.use("/users",userRoutes);
app.use("/courses",courseRoutes);


app.listen(process.env.PORT || 4000, ()=>{

	console.log(`API is now online on port ${process.env.PORT || 4000}`)

})