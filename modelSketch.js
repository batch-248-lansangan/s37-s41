/*

	App: Booking System API

	Scenario:
		A course booking system application where a user can enroll into a course

	Type: Course Booking System (Web App)	

	Description:

		A course booking system application where a user can enroll into a course
		Allows an admin to do CRUD operations
		Allows users to register into our database

	Features: 
		-User Registration
		-User Authentication (User Login)	
		
		Customer/Authenticated Users:
		-View Courses (All Active Courses)
		-Enroll Course

		Admin Users:
		-Add Course
		-Updated Course
		-Archive/Unarchive Course (soft delete/reactivate the course)
		-View Course (All courses ACTIVE/INACTIVE)
		-View/Manager User Accounts**s

		All Users (guests, customers, admin)
		-View all ACTIVE courses


*/

//Data Model for the Booking System
//Two-way Embedding

/*
	user {
	
	id - unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollments: [
		
		id - document identifier
		courseId - the unique identifier for course
		courseName - optional
		status,
		dateEnrolled - optional

		]
	}


*/


/*
	course {
	
	id - unique identifier for the document
	name,
	description,
	price,
	isActive,
	createdOn,
	enrollees :[
		
		id - document identifier
		userId,
		isPaid,
		dateEnrolled - optional

	]


	}


*/